MVP outlook:

 1. [X] A menu to select the preferred installation method. (starting with just the [AppImage](https://appimage.org/))
 2. [X] Download the AppImage into a temporary folder (like "/tmp"), with either 'wget' or a rust crate.
 3. [X] Dump it into the installation location (ex.: "/usr/bin") when the download is finished.
 4. [X] Ask if user wants a pre-made '.desktop' file to be deployed into "~/.local/share/applications" for system integration.

Nice slice-of-life features, but not essential:

 1. [X] Let users customize some settings in a toml config file.
 2. [] Ask user if they want an icon for their app, and if so, download the image to "~/Images/Icons/app.ico", and modify the '.desktop' file to integrate the icon.
 3. [X] Make AppImage executable on unix after installation.
 ?. Apparently there are performance patches and fixes from the Linux community? (source code patching, so out of luck for AppImage) I'll have to look into that.

Edge cases features, kinda nice, but easily ignored:

 1. [] When user is warned that their installation location is not included in PATH, ask if they want to add it to PATH, and ask for confirmation.
 2. [] Add changing the AppImage's, icon and '.desktop' file's locations as options, in a sub-menu, visible as an option in the main menu.
 3. [X] Support 'abort', 'exit' and 'quit' commands into any input field. (basically means the same thing, just stops/exits the program)
 4. [X] If user OS is not [Linux](https://www.linux.com/what-is-linux/), give a comprehensive warning and exit the program.
 5. [X] Have exit codes abide by OpenBSD's <[sysexits.h](https://github.com/openbsd/src/blob/master/include/sysexits.h)>.

Code smell kind off stuff (style and superstition in equal measure):

 1. [X] Separate tasks into different functions when reasonable.
 2. [X] Separate functions into modules when reasonable.
 3. [X] Document functions and code blocks.
 4. [X] Implement printlining, for debugging purposes.
 5. [X] Make sure no error is left unhandled. (I know, boilerplate code, but good practice)
 6. [X] Be [FHS](https://www.pathname.com/fhs/pub/fhs-2.3.html) compliant.

Installation methods:

 1. [X] Bare AppImage.
 2. [] Source code compiling. (not totally *sure* if I can implement that, but opens up patching opportunities, so worth the exploration)
 ?. Uhhhh.. AppImageLauncher? I'll make it up as I go.
