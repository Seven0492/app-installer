build:
	cargo build --release

debug-build:
	cargo build

run:
	cargo run --release

debug-run:
	cargo run

install:
	cargo install --locked --path .
