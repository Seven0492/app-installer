use std::cell::RefCell;
use std::io::{stdout, BufWriter, Write};
use std::rc::Rc;
use std::{path::PathBuf, process::exit};

use console::style;
use exitcode::ExitCode;

use crate::file::apps::{App, Apps, InstallMethod};
use crate::file::{self, config::Config};
use crate::process::versioning::set_app_version;
use crate::{APPS, CONFIG, CURRENT_APP, INTERNAL};

use self::internal::Internal;

/// Contains the initializer for internal vars
pub mod internal;
/// Contains the low-level functions necessary for versioning
mod versioning;

/// Extracts the chosen install method from the provided string,
/// takes it into action and returns the path to the downloaded file
pub(crate) fn download_to_temp() -> Result<PathBuf, ExitCode> {
    let binding = fetch_current_app();
    let mut current_app = binding.borrow_mut();

    let mut choice = String::new();

    let methods = match super::io::installation_methods(&current_app) {
        Ok(methods) => methods,
        Err(exit_code) => {
            return Err(exit_code);
        }
    };

    super::io::get(&mut choice, true);

    let mut file_path = PathBuf::new();

    match InstallMethod::from_str(&choice) {
        Some(method) => {
            let binding = fetch_internal();
            let internal = binding.borrow();

            let binding = fetch_config();
            let config = binding.borrow();

            if !methods.contains(&&method) {
                eprintln!(
                    "\n{} isn't available for {}.\n\n",
                    method, &current_app.name
                );

                return Err(exitcode::OK);
            }

            if method == InstallMethod::Executable {
                if internal.arguments.verbose {
                    println!(
                        "\nChose installation method '{}'.",
                        super::io::format_install_methods(vec![&InstallMethod::Executable], false)
                    );
                }

                file_path = match file::download::download_in_temp(&mut current_app, &config) {
                    Ok(path) => path,
                    Err(exit_code) => {
                        return Err(exit_code);
                    }
                };
            }

            if method == InstallMethod::Compile {
                if internal.arguments.verbose {
                    println!(
                        "\nChose installation method '{}'.",
                        super::io::format_install_methods(vec![&InstallMethod::Compile], false)
                    );
                }

                todo!();
            }
        }
        None => {
            eprintln!("\nYou have entered invalid option '{}'.\n\n", &choice);

            return Err(exitcode::DATAERR);
        }
    };

    Ok(file_path)
}

/// Querries the user to choose an app and returns it
pub(crate) fn get_current_app() -> Result<App, ExitCode> {
    let binding = fetch_internal();
    let internal = binding.borrow();

    let binding = fetch_apps();
    let binding2 = binding.borrow_mut();
    let apps = binding2.get();

    let mut current_app = App::default();

    if internal.arguments.app.is_some() {
        let app_name = internal.arguments.app.clone().unwrap();
        let lowercase_app_name = app_name.to_lowercase();

        for app in apps {
            if app.name.to_lowercase() == lowercase_app_name {
                current_app = app.clone();
            }
        }

        if current_app.name.is_empty() {
            eprintln!(
                "Error: provided invalid app name '{}' as an argument.",
                app_name
            );

            return Err(exitcode::USAGE);
        }
    } else if internal.arguments.apps {
        {
            let mut writer = BufWriter::new(stdout().lock());

            if internal.arguments.debug {
                println!();
            }

            for app in apps {
                if internal.arguments.debug {
                    dbg!(&app.name);

                    println!();
                }

                if writer
                    .write_all(format!("{}\n", &app.name).as_bytes())
                    .is_err()
                {
                    return Err(exitcode::UNAVAILABLE);
                }
            }
        }

        exit(exitcode::OK);
    } else {
        println!(
            "App to install or update {}id{}:",
            style("[").bold(),
            style("]").bold()
        );

        {
            let mut writer = BufWriter::new(stdout().lock());

            if internal.arguments.debug {
                println!();
            }

            for (index, app) in apps.iter().enumerate() {
                if internal.arguments.debug {
                    dbg!(&index);
                    dbg!(&app.name);

                    println!();
                }

                if writer
                    .write_all(format!("  {}. {}\n", &index, &app.name).as_bytes())
                    .is_err()
                {
                    exit(exitcode::UNAVAILABLE);
                }
            }
        }

        let mut buffer = String::new();

        super::io::get(&mut buffer, true);

        println!();

        current_app = match buffer.parse::<usize>() {
            Ok(value) => {
                if value >= apps.len() {
                    eprintln!("Provided invalid id '{}'.", &value);

                    return Err(exitcode::DATAERR);
                }

                apps[value].clone()
            }
            Err(err) => {
                eprintln!(
                    "Error: {}\n\
                    \n\
                    An invalid number was provided.",
                    &err
                );

                return Err(exitcode::DATAERR);
            }
        };
    }

    set_app_version(&mut current_app);

    let empty = &"".to_string();
    let version: &String;

    if current_app.version == None {
        version = empty;
    } else {
        version = current_app.version.as_ref().unwrap();
    }

    if version.contains("latest") && !internal.arguments.force {
        let version = version.split(':').collect::<Vec<&str>>()[1].trim();

        println!(
            "{} is already up-to-date! ({})",
            &current_app.name, &version
        );

        exit(exitcode::OK);
    } else if version.contains("behind") {
        let current_version = version.split(':').collect::<Vec<&str>>()[1].trim();
        let new_version = version.split(':').collect::<Vec<&str>>()[2].trim();

        if !current_version.is_empty() && current_version.to_lowercase() != "none" {
            println!(
                "Installing will update from '{}' to '{}'\n",
                &current_version, &new_version
            );
        }
    }

    Ok(current_app)
}

/// Returns a reference to the available apps
pub(crate) fn fetch_apps() -> Rc<RefCell<Apps>> {
    APPS.with(|apps_cell| Rc::clone(apps_cell))
}

/// Returns a reference to the current id
pub(crate) fn fetch_current_app() -> Rc<RefCell<App>> {
    CURRENT_APP.with(|app_cell| Rc::clone(app_cell))
}

/// Returns a reference to loaded config
pub(crate) fn fetch_config() -> Rc<RefCell<Config>> {
    CONFIG.with(|config_cell| Rc::clone(config_cell))
}

/// Returns a reference to initialized internal variables
pub(crate) fn fetch_internal() -> Rc<RefCell<Internal>> {
    INTERNAL.with(|internal_cell| Rc::clone(internal_cell))
}
