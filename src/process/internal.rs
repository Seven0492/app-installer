use clap::Parser;

use crate::Arguments;

/// Contains all the purely internal settings or information
pub struct Internal {
    /// Arguments passed in from the user
    pub arguments: Arguments,
}

impl Default for Internal {
    fn default() -> Self {
        Self {
            arguments: Arguments::default(),
        }
    }
}

/// Returns an initialized struct containing internal variables
pub(crate) fn initialize_internal() -> Internal {
    let mut internal = Internal::default();

    internal.arguments = Arguments::parse();

    internal
}
