use std::process::exit;

use crate::file::{
    apps::App,
    network::{self, ApiType},
};

use super::{fetch_internal, internal::Internal};

/// Sets the current app's version
pub(crate) fn set_app_version(app: &mut App) {
    let binding = fetch_internal();
    let internal = binding.borrow();

    if app.api == None {
        return;
    }

    if let Some(api_type) = &app.api {
        let link_template: String;
        let repo_owner: String;
        let repo_name: String;
        let link: &mut String;

        let latest_version = match api_type {
            ApiType::Github => {
                link_template = app.download_link_template.clone();
                link = app.get_mut_download_link();

                (repo_owner, repo_name) = extract_repo_from_link(&link_template);

                if internal.arguments.debug {
                    dbg!(&link_template);
                    dbg!(&repo_owner);
                    dbg!(&repo_name);
                }

                network::get_latest_github_version(&repo_owner, &repo_name)
            }
        };

        if internal.arguments.debug {
            dbg!(&latest_version);
        }

        (*link) = link_template
            .replace(['(', ')'], "")
            .replace("{version}", &latest_version);

        if internal.arguments.debug {
            dbg!(&link);

            if internal.arguments.verbose {
                eprintln!();
            }
        }

        set_version(app, &internal, latest_version);

        if internal.arguments.debug {
            dbg!(&app.version);

            println!();
        }
    } else {
        unreachable!();
    }
}

/// Sets version in config
pub(crate) fn set_version(app: &mut App, internal: &Internal, latest_version: String) {
    if let None = app.version {
        app.version = Some(format!(""));
    }

    let raw_version = app.version.as_ref().unwrap();

    if internal.arguments.debug {
        dbg!(&raw_version);
    }

    let current_version = extract_current_version(raw_version);

    if internal.arguments.debug {
        dbg!(&current_version);
    }

    if latest_version.is_empty() {
        app.version = None;

        return;
    }

    if latest_version == current_version.trim() {
        app.version = Some(format!("latest: {}", &latest_version));
    } else if !current_version.is_empty() && !current_version.contains("none") {
        app.version = Some(format!(
            "behind: {}:{}",
            &current_version.clone(),
            &latest_version
        ));
    } else {
        app.version = Some(format!("none: {}", &latest_version));
    }
}

/// Extracts the repo name and owner from a link, and returns it as a tuple
///
/// (repo_owner, repo_name)
pub fn extract_repo_from_link(link: &str) -> (String, String) {
    if !link.contains('(') || !link.contains(')') {
        eprintln!(
            "Faulty template download link: '{}'\n\
            \n\
            Doesn't contain any '()' pairs (2 required; check documentation).",
            &link
        );

        exit(exitcode::DATAERR);
    }

    let mut repo_owner = "";
    let mut repo_name = "";

    for (index, section) in link.split('(').enumerate() {
        if index == 1 {
            repo_owner = section;
        } else if index == 2 {
            repo_name = section;
        } else if index != 0 {
            break;
        }
    }

    for (index, section) in <&str>::clone(&repo_owner).split(')').enumerate() {
        if index == 0 {
            repo_owner = section;
        } else {
            break;
        }
    }

    for (index, section) in <&str>::clone(&repo_name).split(')').enumerate() {
        if index == 0 {
            repo_name = section;
        } else {
            break;
        }
    }

    if repo_owner.is_empty() {
        eprintln!(
            "Faulty template download link: '{}'\n\
            \n\
            Detected an empty '()' pair (minimum 1 character).",
            &link
        );

        exit(exitcode::DATAERR);
    } else if repo_name.is_empty() {
        eprintln!(
            "Faulty template download link: '{}'\n\
            \n\
            Detected an empty or missing '()' pair (2 required; check documentation).",
            &link
        );

        exit(exitcode::DATAERR);
    }

    (repo_owner.to_string(), repo_name.to_string())
}

/// Extracts and returns the current versions from the raw version provided
pub fn extract_current_version(raw_version: &String) -> String {
    if raw_version.contains("latest")
        || raw_version.contains("behind")
        || raw_version.contains("none:")
    {
        return raw_version.split(':').collect::<Vec<&str>>()[1]
            .trim()
            .to_string();
    }

    return raw_version.trim().to_string();
}
