use file::{
    apps::{App, Apps},
    config::Config,
};
use std::{cell::RefCell, rc::Rc};

use clap::Parser;
use exitcode::ExitCode;
use process::{
    fetch_apps, fetch_config, fetch_current_app, fetch_internal, get_current_app,
    internal::Internal,
};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Arguments {
    /// Choose an app to install (case insensitive)
    #[arg(long, short, value_name = "APP_NAME")]
    app: Option<String>,

    /// Print a list of all app options
    #[arg(long)]
    apps: bool,

    /// Enable verbose prints
    #[arg(long, short)]
    verbose: bool,

    /// Enable debug info
    #[arg(long, short)]
    debug: bool,

    /// Force install an app
    #[arg(long, short)]
    force: bool,
}

impl Default for Arguments {
    fn default() -> Self {
        Self {
            app: None,
            apps: false,
            verbose: false,
            debug: false,
            force: false,
        }
    }
}

thread_local! {
    /// Mutable static containing the internal settings of the program
    static INTERNAL: Rc<RefCell<Internal>> = Rc::new(RefCell::new(Internal::default()));
}

thread_local! {
    /// Mutable static containing the configuration of the program
    static CONFIG: Rc<RefCell<Config>> = Rc::new(RefCell::new(Config::default()));
}

thread_local! {
    /// Mutable static containing the configuration of the program
    static APPS: Rc<RefCell<Apps>> = Rc::new(RefCell::new(Apps::default()));
}

thread_local! {
    /// Immutable static containing the id of the app that the user selected
    static CURRENT_APP: Rc<RefCell<App>> = Rc::new(RefCell::new(App::default()));
}

/// Contains anything file-related
mod file;
/// Contains functions related to I/O with the user
mod io;
/// Contains generic os functions
mod os;
/// Contains general functions
mod process;

fn main() -> Result<(), ExitCode> {
    if !cfg!(unix) {
        crate::os::warning();
    };

    match initialize_globals() {
        Ok(_) => (),
        Err(exit_code) => {
            return Err(exit_code);
        }
    };

    let file_path = match process::download_to_temp() {
        Ok(path) => path,
        Err(exit_code) => {
            return Err(exit_code);
        }
    };

    let exe_location = file::install_file(&file_path);

    if file::integration::deploy(&exe_location).is_err() {
        eprintln!("\nCouldn't deploy integration.");
    }

    file::apps::write_app(&mut fetch_current_app().borrow_mut());

    #[cfg(unix)]
    os::desktop::refresh_cache();

    println!("\nFinished installation!");

    Ok(())
}

/// Initializes global variables in a procedural order
fn initialize_globals() -> Result<(), ExitCode> {
    let binding = fetch_internal();
    let mut internal = binding.borrow_mut();

    let binding = fetch_apps();
    let mut apps = binding.borrow_mut();

    let binding = fetch_config();
    let mut config = binding.borrow_mut();

    let binding = fetch_current_app();
    let mut current_app = binding.borrow_mut();

    *internal = process::internal::initialize_internal();

    drop(internal);

    *config = file::config::load();

    drop(config);

    *apps = file::apps::get_apps();

    drop(apps);

    *current_app = match get_current_app() {
        Ok(app) => app,
        Err(exit_code) => {
            return Err(exit_code);
        }
    };

    Ok(())
}
