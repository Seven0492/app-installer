use std::io::{stdin, stdout, Write};
use std::process::exit;

use console::style;
use exitcode::ExitCode;

use crate::file::apps::{App, InstallMethod};
use crate::process::fetch_internal;

/// Querries input from the user
pub fn get(buffer: &mut String, loop_if_empty: bool) {
    loop {
        print!(" ==> ");

        // Flushes (or 'clears') standard output. (to prevent an unwanted newline after print)
        if let Err(err) = stdout().flush() {
            eprintln!("Error: {} (failed to flush stdout)", &err);
        }

        // Captures input from standard input until the 'enter' key is pressed.
        if let Err(err) = stdin().read_line(buffer) {
            eprintln!("\n\nError: {} (failed to capture stdin)\n", &err);

            exit(exitcode::IOERR);
        }

        *buffer = buffer.trim().to_string();

        if loop_if_empty && buffer.is_empty() {
            continue;
        }

        if is_exiting(buffer) {
            exit(exitcode::OK);
        }

        break;
    }
}

/// Gets lowercase input from the user
pub fn get_lower(buffer: &mut String, loop_if_empty: bool, truncate: bool) {
    get(buffer, loop_if_empty);

    *buffer = buffer.to_lowercase();

    if truncate {
        buffer.truncate(1);
    }
}

/// Checks if buffer matches any keywords associated with program closure
fn is_exiting(buffer: &str) -> bool {
    let buffer = buffer.trim().to_lowercase();

    if buffer == "exit" || buffer == "quit" || buffer == "abort" {
        return true;
    }

    false
}

/// Extracts the installation methods compatible with the chosen app,
/// prints it and returns the resulting vector of `InstallMethods`
pub(crate) fn installation_methods(app: &App) -> Result<Vec<&InstallMethod>, ExitCode> {
    let binding = fetch_internal();
    let internal = binding.borrow();

    if internal.arguments.debug {
        dbg!(&app.install_methods);

        eprintln!();
    }

    if app.install_methods.is_empty() {
        eprintln!("Error: no install methods specified for {}.", &app.name);

        return Err(exitcode::DATAERR);
    }

    println!(
        "Installation methods: {}",
        format_install_methods(app.install_methods.get(), true)
    );

    Ok(app.install_methods.get())
}

/// Takes a reference to a vector of `InstallMethods` and returns it formatted as a `String`
pub fn format_install_methods(install_methods: Vec<&InstallMethod>, for_prompt: bool) -> String {
    let mut buffer = String::new();

    let last_index = install_methods.len() - 1;

    for (index, method) in install_methods.iter().enumerate() {
        if index == 0 {
            if **method == InstallMethod::Executable {
                buffer.push_str("Standalone executable");
            } else if **method == InstallMethod::Compile {
                buffer.push_str("Source code compiling");
            }
        } else if **method == InstallMethod::Executable {
            buffer.push_str("standalone executable");
        } else if **method == InstallMethod::Compile {
            buffer.push_str("source code compiling");
        }

        if for_prompt {
            let buffer_2 = buffer.split_whitespace().collect::<Vec<&str>>();

            let last_word = buffer_2.last().unwrap();

            let buffer_2 = {
                let buffer_lenght = buffer_2.len();
                let mut buffer = String::with_capacity(buffer_lenght);

                for (index, word) in buffer_2.iter().enumerate() {
                    if index != buffer_lenght - 1 {
                        buffer.push_str(word);
                        buffer.push(' ');
                    }
                }

                buffer
            };

            let mut last_word_2 = String::new();

            for (index, character) in last_word.char_indices() {
                if index == 0 {
                    last_word_2.push_str(&style("[").bold().to_string());
                    last_word_2.push(character);
                    last_word_2.push_str(&style("]").bold().to_string());

                    continue;
                }

                last_word_2.push(character);
            }

            buffer = format!("{}{}", &buffer_2, &last_word_2);
        }

        if index != last_index {
            buffer.push_str(", ");
        }
    }

    buffer
}
