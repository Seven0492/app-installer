use std::process::{exit, Command};

use crate::process::fetch_internal;

/// Clones a git repo into a directory
pub fn _fetch_repo(link: &str, dir: &str) {
    let binding = fetch_internal();
    let internal = binding.borrow();

    let mut program = Command::new("git");

    let command = program.current_dir(dir).args(["clone", link]);

    if internal.arguments.debug {
        println!("\nGit command: '{:?}'\n\n", &command);
    }

    let mut child = match command.spawn() {
        Ok(spawn_child) => spawn_child,
        Err(err) => {
            eprintln!(
                "\nError: {} (ex.: 'git' utility missing from system)\n\n",
                &err
            );

            exit(exitcode::TEMPFAIL);
        }
    };

    match child.wait() {
        Ok(exit_code) => {
            if exit_code.success() {
                if internal.arguments.verbose {
                    println!("\nSuccessfully cloned git repo.\n\n");
                }
            } else {
                eprintln!("\nFailed to fetch git repo.\n\n");

                exit(exitcode::UNAVAILABLE);
            }
        }
        Err(err) => {
            eprintln!(
                "\nError: {} (ex.: `git` utility missing from system)\n\n",
                &err
            );

            exit(exitcode::TEMPFAIL);
        }
    };
}
