use std::{
    fs::{self, File},
    path::{Path, PathBuf},
    process::exit,
};

/// Contains anything app-specifig.
pub mod apps;
/// Contains anything configuration-related
pub mod config;
/// Contains the functions that are download-specific
pub mod download;
/// Contains git-related functions
pub mod git;
/// Contains anything related to the integration of apps into an environment
pub mod integration;
/// Contains anything API-related
pub mod network;

/// Contains the functions that are unix-specific
#[cfg(unix)]
pub mod unix;

use console::style;
use exitcode::ExitCode;

use crate::{fetch_config, fetch_current_app, io, process::fetch_internal};
use config::Config;

/// Finds the home directory of the current user
/// and returns it
pub fn home_dir() -> PathBuf {
    let user_dirs = match directories::UserDirs::new() {
        Some(user_dirs) => user_dirs,
        None => {
            eprintln!("\nError: failed to detect home directory.\n");

            let mut buffer = String::new();

            println!("Please specify the home directory (or its equivalent; else program cannot function):");

            io::get(&mut buffer, false);

            if !buffer.is_empty() {
                return PathBuf::from(buffer);
            }

            println!();

            exit(exitcode::NOUSER);
        }
    };

    user_dirs.home_dir().to_path_buf()
}

/// Moves target file to destination,
/// also optionally deletes target after move
pub fn move_file(target: &Path, destination: &mut PathBuf, delete_target: &bool) {
    let binding = fetch_internal();
    let internal = binding.borrow();

    if internal.arguments.verbose {
        println!(
            "Now moving target ('{}') to destination ('{}').",
            &target.display(),
            &destination.display()
        );
    }

    let file_name = match destination.components().last() {
        Some(name) => name.as_os_str().to_string_lossy().to_string(),
        None => {
            eprintln!(
                "'{}' is an invalid installation location!\n\n",
                &destination.display()
            );

            exit(exitcode::CONFIG);
        }
    };

    if internal.arguments.debug {
        dbg!(&file_name);
    }

    destination.pop();

    if let Err(err) = fs::create_dir_all(&destination) {
        if err.to_string() == "Permission denied (os error 13)" {
            permission_denied(target, destination, delete_target);

            return;
        } else {
            eprintln!(
                "\nError: {}\n\
                \n\
                Failed to create the directories to the installation \
                location.\n",
                &err
            );
        }

        exit(exitcode::CANTCREAT);
    }

    destination.push(file_name);

    copy(target, destination, delete_target);

    if *delete_target {
        if let Err(err) = fs::remove_file(target) {
            eprintln!(
                "\nError: {}\n\
                \n\
                Failed to remove target after installation.\n",
                &err
            );

            exit(exitcode::UNAVAILABLE);
        }
    }

    if internal.arguments.verbose {
        println!("\nSuccessfully moved file.\n");
    }
}

/// Copies target to destination
fn copy(target: &Path, destination: &mut PathBuf, delete_target: &bool) {
    loop {
        if let Err(err) = fs::copy(target, &destination) {
            if err.to_string().contains("os error 13") {
                permission_denied(target, destination, delete_target);

                return;
            } else if err.to_string().contains("os error 26") {
                eprintln!(
                    "\nError: cannot replace program with newer version when it's running.\n"
                );

                println!(
                    "Please confirm when you have closed the program {}{}/n{}:",
                    style("[").bold(),
                    style("Y").yellow(),
                    style("]").bold()
                );

                let mut buffer = String::new();

                io::get_lower(&mut buffer, false, true);

                if buffer != "n" {
                    println!();

                    continue;
                } else {
                    exit(exitcode::OK);
                }
            } else {
                eprintln!(
                    "\nError: {}\n\
                    \n\
                    Failed to copy target to installation location.\n",
                    &err
                );
            }

            exit(exitcode::UNAVAILABLE);
        } else {
            break;
        }
    }
}

/// Offers the option to change installation location
/// after being denied access to the default one
fn permission_denied(target: &Path, destination: &mut PathBuf, delete_target: &bool) {
    let mut buffer = String::new();

    let binding = fetch_config();
    let config = binding.borrow();

    let binding = fetch_current_app();
    let current_app = binding.borrow();

    println!(
        "\nThe program does not have the necessary permissions to install to '{}'.\n\
        \n\
        Do you want to:\n\
        |   1. try again in '{}' ?\n\
        |   2. try again in '{}' ?\n\
        |   3. try again in '{}' ?\n\
        |   4. define the installation location yourself ?",
        &destination.display(),
        &config
            .app_location
            .clone()
            .unwrap_or(Config::default().app_location.unwrap()),
        &config
            .install_location
            .clone()
            .unwrap_or(Config::default().install_location.unwrap()),
        &config
            .local_install_location
            .clone()
            .unwrap_or(Config::default().local_install_location.unwrap())
    );

    super::io::get_lower(&mut buffer, true, true);

    if buffer == "1" {
        *destination = PathBuf::from(
            &config
                .app_location
                .clone()
                .unwrap_or(Config::default().app_location.unwrap()),
        );

        destination.push(&current_app.name);
        destination.push("bin");
    } else if buffer == "2" {
        *destination = PathBuf::from(
            &config
                .install_location
                .clone()
                .unwrap_or(Config::default().install_location.unwrap()),
        );

        destination.push(&current_app.name);
    } else if buffer == "3" {
        *destination = PathBuf::from(
            &config
                .local_install_location
                .clone()
                .unwrap_or(Config::default().local_install_location.unwrap()),
        );

        destination.push(&current_app.name);
    } else if buffer == "4" {
        loop {
            println!("\nInstallation location:");

            super::io::get(&mut buffer, true);

            let str_destination = buffer.clone();

            println!(
                "\nConfirm installation location {}y/{}{}:",
                style("[").bold(),
                style("N").yellow(),
                style("]").bold()
            );

            super::io::get_lower(&mut buffer, true, true);

            if buffer == "y" {
                move_file(target, &mut PathBuf::from(str_destination), delete_target);

                return;
            } else {
                continue;
            }
        }
    } else {
        eprintln!("\nInvalid option number '{}'.", &buffer);

        exit(exitcode::DATAERR);
    }

    move_file(target, destination, delete_target);
}

/// Moves executable from download location to installation location
pub(crate) fn install_file(target: &Path) -> PathBuf {
    let binding = fetch_config();
    let config = binding.borrow();

    let binding = fetch_current_app();
    let current_app = binding.borrow();

    let mut exe_location = PathBuf::from(
        &config
            .local_install_location
            .clone()
            .unwrap_or(Config::default().local_install_location.unwrap()),
    );

    exe_location.push(&current_app.get_filtered_name());

    move_file(target, &mut exe_location, &true);

    #[cfg(unix)]
    if cfg!(unix) {
        if super::file::unix::make_executable(&exe_location).is_err() {
            eprintln!("\nFailed to make file executable.");
        }
    }

    exe_location
}

pub fn new_file(path: impl Into<PathBuf>) -> Result<File, ExitCode> {
    let path = path.into();

    match File::create(path) {
        Ok(file) => Ok(file),
        Err(err) => {
            eprintln!(
                "Error: {}\n\
                \n\
                Failed to create file.",
                &err
            );

            return Err(exitcode::CANTCREAT);
        }
    }
}
