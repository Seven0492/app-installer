use std::path::Path;
use std::process::{exit, Command};

use crate::process::fetch_internal;

use exitcode::ExitCode;

/// Makes a file executable
#[cfg(unix)]
pub fn make_executable(file_path: &Path) -> Result<(), ExitCode> {
    let binding = fetch_internal();
    let internal = binding.borrow();

    let mut program = Command::new("chmod");

    let command = program.args([
        "u+x",
        file_path.to_str().unwrap_or_else(|| {
            eprintln!("App file path is not valid unicode!");

            exit(exitcode::CONFIG);
        }),
    ]);

    if internal.arguments.debug {
        println!("\nCommand: {:?}\n\n", &command);
    }

    let mut child = match command.spawn() {
        Ok(spawn_child) => spawn_child,
        Err(err) => {
            eprintln!(
                "\nError: {} (ex.: 'chmod' utility missing from system)\n\n",
                &err
            );

            return Err(exitcode::TEMPFAIL);
        }
    };

    match child.wait() {
        Ok(_) => {}
        Err(err) => {
            eprintln!("\nError: {} (ex.: failed to change permissions)\n\n", &err);

            return Err(exitcode::NOPERM);
        }
    };

    Ok(())
}
