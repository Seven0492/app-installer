// use std::fs::write;
// use std::path::{Path, PathBuf};

// use serde::{Deserialize, Serialize};

// use crate::prelude::DEFAULT_LINUX_CACHE_LOCATION;
// use crate::process::{fetch_config, fetch_internal};

// /// Contains information that is not essential for runtime but shouldn't be touched by the user
// #[derive(Deserialize, Debug, Serialize)]
// pub struct Cache {
//     /// Contains the versions of the already installed software, if any
//     pub versions: Option<Vec<String>>,
// }

// impl Default for Cache {
//     fn default() -> Self {
//         Self {
//             versions: Some(vec!["".to_string()]),
//         }
//     }
// }

// /// Gets the cache from the filesystem and returns the `Result`.
// pub fn get_cache() -> Result<Cache, ()> {
//     let binding = fetch_config();
//     let config = binding.borrow();

//     let file = std::fs::read_to_string(
//         DEFAULT_LINUX_CACHE_LOCATION.replace('~', &config.home_dir.clone().unwrap()),
//     );

//     let cache = match file {
//         Ok(value) => match toml::from_str(&value) {
//             Ok(cache) => cache,
//             Err(err) => {
//                 eprintln!(
//                     "Error: {}\n\
//                     \n\
//                     Cache file has been corrupted; invalid toml file.",
//                     &err
//                 );

//                 return Err(());
//             }
//         },
//         Err(err) => {
//             eprintln!(
//                 "Error: {}\n\
//                 \n\
//                 Failed to read from cache (necessary for version related tasks).",
//                 &err
//             );

//             return Err(());
//         }
//     };

//     Ok(cache)
// }

// /// Writes to cache
// pub fn write_cache(file_path: &Path, cache: Cache) {
//     let binding = fetch_internal();
//     let internal = binding.borrow();

//     let iterator = file_path.display().to_string();

//     let last_item = iterator.split('/').count() - 1;

//     let mut dir = PathBuf::from("/");

//     for (index, section) in iterator.split('/').enumerate() {
//         if index != last_item {
//             dir.push(section);
//         }
//     }

//     if let Err(err) = std::fs::create_dir_all(dir) {
//         eprintln!(
//             "\nError: {}\n\
//             \n\
//             Failed to create the cache directory.\n\n",
//             &err
//         );

//         return;
//     }

//     let mut new_cache = Cache {
//         versions: Some({
//             let mut vec: Vec<String> = Vec::new();

//             for _ in 0..cache.versions.clone().unwrap().len() {
//                 vec.push("".to_string());
//             }

//             vec
//         }),
//     };

//     let mut versions: Vec<String> = Vec::new();

//     if let Some(cache_versions) = cache.versions {
//         versions = cache_versions;
//     }

//     if internal.arguments.debug == true {
//         dbg!(&versions);
//     }

//     for (index, version) in versions.iter().enumerate() {
//         if let Some(vector) = new_cache.versions.as_mut() {
//             if version.contains("none:") || version.contains("latest") {
//                 vector[index] = version.split(':').collect::<Vec<&str>>()[1]
//                     .trim()
//                     .to_string();
//             } else if version.contains("behind") {
//                 vector[index] = version.split(':').collect::<Vec<&str>>()[2]
//                     .trim()
//                     .to_string();
//             } else {
//                 vector[index] = version.clone();
//             }
//         }
//     }

//     let file = match toml::to_string(&new_cache) {
//         Ok(data) => data,
//         Err(err) => {
//             eprintln!(
//                 "\nError: {}\n\
//                 \n\
//                 Failed to parse cache into toml format.",
//                 &err
//             );

//             "".to_string()
//         }
//     };

//     if !file.is_empty() {
//         if let Err(err) = write(file_path, file) {
//             eprintln!(
//                 "\nError: {}\n\
//                 \n\
//                 Failed to write cache to the cache file.\n\n",
//                 &err
//             );
//         };
//     } else {
//         eprintln!("\nPassed an empty `Cache` struct to `write_cache`!\n");
//     }

//     if internal.arguments.debug == true {
//         dbg!(&new_cache);

//         println!();
//     }

//     if internal.arguments.verbose {
//         println!("\nSuccessfully saved cache!\n");
//     }
// }

// /// Writes versions in config to cache
// pub(crate) fn write_versions_to_cache() {
//     write_cache(
//         &PathBuf::from(
//             DEFAULT_LINUX_CACHE_LOCATION
//                 .replace('~', &fetch_config().borrow().home_dir.clone().unwrap()),
//         ),
//         Cache {
//             versions: fetch_internal().borrow().versions.clone(),
//         },
//     );
// }
