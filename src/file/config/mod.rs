use directories::ProjectDirs;
use serde::{Deserialize, Serialize};
use std::{
    fs::read_to_string,
    fs::write,
    path::{Path, PathBuf},
};

use crate::process::fetch_internal;

// /// Contains anything specific to the cache
// pub mod cache;

#[derive(Deserialize, Debug, Serialize)]
pub struct Config {
    /// Directory that houses `.desktop` files, preferrably user-specific
    pub desktop_file_location: Option<String>,

    /// Directory for temporary files, with read-write access
    pub download_location: Option<String>,

    /// App specific installation directory
    pub app_location: Option<String>,

    /// Non user-specific installation directory
    pub install_location: Option<String>,

    /// User-specific installation directory
    pub local_install_location: Option<String>,

    /// Home directory of the user
    pub home_dir: Option<String>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            desktop_file_location: Some(format!(
                "{}/.local/share/applications",
                super::home_dir().display()
            )),
            download_location: Some("/tmp".to_string()),
            app_location: Some("/opt/app-installer".to_string()),
            install_location: Some("/usr/local/bin".to_string()),
            local_install_location: Some(format!("{}/.local/bin", super::home_dir().display())),
            home_dir: Some(format!("{}", super::home_dir().display())),
        }
    }
}

/// Returns `Config` from config file, or, in the case of an error, returns the default
pub fn load() -> Config {
    let binding = fetch_internal();
    let internal = binding.borrow();

    match ProjectDirs::from("dev", "installer", "app-installer") {
        Some(proj_dirs) => {
            let config_dir = proj_dirs.config_dir();

            let config_file = read_to_string(config_dir.join("config.toml"));

            println!(
                "Extensive configuration is available at '{}'.\n",
                &config_dir.join("config.toml").display()
            );

            match config_file {
                Ok(file) => toml::from_str(&file).unwrap_or_else(|err| {
                    if internal.arguments.verbose {
                        eprintln!(
                            "Error: {}\n\
                            \n",
                            &err
                        );
                    }

                    if err.to_string().contains("os error 2") {
                        eprintln!("Missing config file, reverting to default.\n");
                    } else {
                        eprintln!("Failed to parse config file, reverting to default config.\n");
                    }

                    if internal.arguments.verbose {
                        println!("Attempting to write default config to config directory.\n");
                    }

                    super::move_file(
                        &config_dir.join("config.toml"),
                        &mut config_dir.join("config.toml.bak"),
                        &true,
                    );

                    write_config(&Config::default(), &config_dir.join("config.toml"));

                    Config::default()
                }),
                Err(err) => {
                    if internal.arguments.verbose {
                        eprintln!(
                            "Error: {}\n\
                            \n",
                            &err
                        );
                    }

                    if err.to_string().contains("os error 2") {
                        eprintln!("Missing config file, reverting to default.\n");

                        if internal.arguments.verbose {
                            println!("Attempting to write default config to config directory.\n");
                        }

                        write_config(&Config::default(), &config_dir.join("config.toml"));
                    } else {
                        eprintln!("Failed to parse config file, reverting to default config.\n");
                    }

                    Config::default()
                }
            }

            // Linux:   /home/USER/.config/app-installer
            // Windows: C:\Users\USER\AppData\Roaming\installer\app-installer
            // macOS:   /Users/USER/Library/Application Support/dev.installer.app-installer
        }
        None => {
            eprintln!("Failed to detect config directory, reverting to default config.\n");

            Config::default()
        }
    }
}

/// Writes to config
pub fn write_config(config: &Config, file_path: &Path) {
    let binding = fetch_internal();
    let internal = binding.borrow();

    let iterator = file_path.display().to_string();

    let last_item = iterator.split('/').count() - 1;

    let mut dir = PathBuf::from("/");

    for (index, section) in iterator.split('/').enumerate() {
        if index != last_item {
            dir.push(section);
        }
    }

    if let Err(err) = std::fs::create_dir_all(dir) {
        eprintln!(
            "\nError: {}\n\
            \n\
            Failed to create the config directory.\n\n",
            &err
        );

        return;
    }

    let file = match toml::to_string(config) {
        Ok(data) => data,
        Err(err) => {
            eprintln!(
                "\nError: {}\n\
                \n\
                Failed to parse config into toml format.",
                &err
            );

            "".to_string()
        }
    };

    if !file.is_empty() {
        if let Err(err) = write(file_path, file) {
            eprintln!(
                "\nError: {}\n\
                \n\
                Failed to write config to the config file.\n\n",
                &err
            );
        };
    } else {
        eprintln!("Passed an empty `Config` struct to `write_config`!\n");
    }

    if internal.arguments.verbose {
        println!("Successfully saved configuration!\n");
    }
}
