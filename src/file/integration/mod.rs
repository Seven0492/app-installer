use std::{
    fs::{create_dir_all, write},
    ops::Add,
    path::{Path, PathBuf},
};

use console::style;
use directories::ProjectDirs;
use exitcode::ExitCode;

use crate::{
    file::config::Config,
    process::{fetch_config, fetch_current_app, fetch_internal, internal::Internal},
};

use super::apps::App;

/// Integrates the app with the desktop environment by
/// providing a `.desktop` file generated from app configuration
pub(crate) fn integrate(exe_location: &Path, current_app: &App) -> PathBuf {
    let binding = fetch_config();
    let config = binding.borrow();

    let binding = fetch_internal();
    let internal = binding.borrow();

    let mut desktop_location =
        PathBuf::from(config.desktop_file_location.clone().unwrap_or_else(|| {
            eprintln!(
                "No `desktop_file_location` field set in config!
                Reverting to default."
            );

            Config::default().desktop_file_location.unwrap()
        }));

    let exe_name = &current_app.name.replace(' ', "_");

    let empty = "".to_string();

    let description = current_app.description.as_ref().unwrap_or(&empty);

    let category = current_app.category.as_ref().unwrap_or(&empty);

    let generic_name = current_app.generic_name.as_ref().unwrap_or(&empty);

    let icon_path = current_app.icon.as_ref().unwrap_or(&empty);

    if internal.arguments.debug {
        println!();

        dbg!(&exe_location);
        dbg!(&exe_name);
        dbg!(&description);
        dbg!(&category);
        dbg!(&generic_name);
        dbg!(&desktop_location);
        dbg!(&icon_path);
    }

    if !desktop_location.exists() {
        if let Err(err) = create_dir_all(&desktop_location) {
            eprintln!(
                "Error: {}\n\
                \n\
                Failed to create desktop file directory.",
                &err
            );

            return PathBuf::new();
        }
    }

    desktop_location.push(current_app.get_filtered_name().clone().add(".desktop"));

    if let Err(err) = write(
        &desktop_location,
        format!(
            "# Desktop Entry Specification: \
            https://standards.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html\n\
            \n\
            [Desktop Entry]\n\
            Encoding=UTF-8\n\
            Type=Application\n\
            {2}\
            Terminal=false\n\
            {3}\
            Exec={0}\n\
            Name={1}\n\
            {4}\
            {5}\
            \n\
            TryExec={0}\n",
            exe_location.display(),
            exe_name,
            format_key_pair("Comment", &description, &internal),
            format_key_pair("Categories", &category, &internal),
            format_key_pair("GenericName", &generic_name, &internal),
            format_key_pair("Icon", &icon_path, &internal),
        ),
    ) {
        eprintln!(
            "\nError: {}\n\
            \n\
            Failed to make a .desktop file.\n",
            &err
        );
    };

    desktop_location
}

/// Deploys integration with desktop environment, after asking user for confirmation
pub fn deploy(exe_location: &Path) -> Result<(), ExitCode> {
    let mut buffer = String::new();

    println!(
        "Do you want to deploy a desktop file (integration) for your \
        desktop environment? {}{}/n{}:",
        style("[").bold(),
        style("Y").yellow(),
        style("]").bold()
    );

    super::io::get_lower(&mut buffer, false, true);

    if buffer == "n" {
        return Ok(());
    }

    let binding = fetch_current_app();
    let mut current_app = binding.borrow_mut();

    if current_app.icon.is_some() {
        current_app.icon = Some(
            get_icon(&current_app.icon.clone().unwrap(), &mut current_app)
                .as_ref()
                .unwrap_or(&PathBuf::new())
                .to_str()
                .expect("Invalid unicode in icon path.")
                .to_string(),
        );
    }

    let desktop_file_location = integrate(&exe_location, &current_app);

    #[cfg(unix)]
    if cfg!(unix) && desktop_file_location.to_str().unwrap().trim() != "" {
        return super::unix::make_executable(&desktop_file_location);
    }

    Ok(())
}

pub fn format_key_pair(key: &str, value: &str, internal: &Internal) -> String {
    let mut key_pair = String::new();

    if !key.is_empty() && !value.is_empty() {
        key_pair = format!("{}={}\n", &key, &value);
    }

    if internal.arguments.debug {
        println!();

        dbg!(&key_pair);
    }

    key_pair
}

pub fn get_icon(icon: &str, mut current_app: &mut App) -> Option<PathBuf> {
    if icon.is_empty() {
        return None;
    }

    let binding = fetch_internal();
    let internal = binding.borrow();

    let icon_link = icon.trim();

    let data_dir = ProjectDirs::from("dev", "installer", "app-installer");

    println!();

    if internal.arguments.debug {
        dbg!(&data_dir);

        println!();
    }

    let mut icon_path = data_dir.as_ref().unwrap().data_local_dir().to_path_buf();

    icon_path.push("assets");

    if !icon_path.exists() {
        if let Err(err) = create_dir_all(&icon_path) {
            eprintln!(
                "Error: {}.\n\
                \n\
                Failed to create data assets directory.\n",
                &err
            );

            return None;
        }
    }

    icon_path.push(
        icon_link
            .split('/')
            .collect::<Vec<&str>>()
            .last()
            .unwrap_or(&&*current_app.get_filtered_name()),
    );

    if icon.contains(':') {
        return match super::download::download(&icon_link, &icon_path, &mut current_app, true) {
            Ok(path) => Some(path),
            Err(err) => {
                eprintln!(
                    "Error code: {}\n\
                    \n\
                    Failed to get app icon.\n",
                    &err
                );

                return None;
            }
        };
    }

    None
}
