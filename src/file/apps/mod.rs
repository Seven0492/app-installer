use std::{
    fmt,
    fs::{create_dir_all, read_to_string, write},
    path::{Path, PathBuf},
    process::exit,
};

use directories::ProjectDirs;
use enum_iterator::{all, Sequence};
use serde::{Deserialize, Serialize};

use crate::process::fetch_internal;

use super::network::ApiType;

pub struct Apps(Vec<App>);

impl Default for Apps {
    fn default() -> Self {
        Self {
            0: vec![
                Apps::osu(),
                Apps::freetube(),
                Apps::protonup_qt(),
                Apps::joplin(),
                // Disabled outside unix since the current configuration scheme doesn't allow os-specific download links.
                //
                // Also debug only because decompression isn't supported yet, like all the others below.
                #[cfg(unix)]
                #[cfg(debug_assertions)]
                Apps::thrive(),
                #[cfg(debug_assertions)]
                Apps::calibre(),
                #[cfg(debug_assertions)]
                Apps::rust_analyzer(),
            ],
        }
    }
}

impl Apps {
    /// Get a vector containing immutable references to all apps
    pub fn get(&self) -> Vec<&App> {
        let mut apps = Vec::new();

        for app in &self.0 {
            apps.push(app);
        }

        apps
    }

    /// Get a vector containing mutable references to all apps
    pub fn _get_mut(&mut self) -> Vec<&mut App> {
        let mut apps = Vec::new();

        for app in &mut self.0 {
            apps.push(app);
        }

        apps
    }

    /// Checks if app's name matches any in the set, and if yes, then it returns it's index.
    pub fn matches(&self, match_app: &App) -> Option<usize> {
        for (index, app) in self.0.iter().enumerate() {
            if match_app.name == app.name {
                return Some(index);
            }
        }

        None
    }

    /// Returns the default app template 'osu!lazer'
    fn osu() -> App {
        App {
            name: "osu!".to_string(),
            generic_name: Some("Rhythm game".to_string()),
            #[cfg(unix)]
            category: Some("Game".to_string()),
            description: Some(
                "A free-to-win rhythm game. Rhythm is just a *click* away!".to_string(),
            ),
            api: Some(ApiType::Github),
            download_link_template:
                "https://github.com/(ppy)/(osu)/releases/download/{version}/osu.AppImage"
                    .to_string(),
            default: true,
            icon: Some(
                "https://raw.githubusercontent.com/ppy/osu/master/assets/lazer.png".to_string(),
            ),
            ..Default::default()
        }
    }

    /// Returns the default app template 'Freetube'
    fn freetube() -> App {
        App {
            name: "Freetube".to_string(),
            generic_name: Some("Youtube client".to_string()),
            #[cfg(unix)]
            category: Some("Network".to_string()),
            description: Some("An open-source, free, and private Youtube client.".to_string()),
            api: Some(ApiType::Github),
            download_link_template: "https://github.com/(FreeTubeApp)/(FreeTube)/releases/download/v{version}-beta/freetube_{version}_amd64.AppImage".to_string(),
            default: true,
            icon: Some("https://raw.githubusercontent.com/FreeTubeApp/FreeTube/development/_icons/icon.ico".to_string()),
            ..Default::default()
        }
    }

    /// Returns the default app template 'thrive'
    fn thrive() -> App {
        App {
            name: "Thrive".to_string(),
            generic_name: Some("Evolution game".to_string()),
            #[cfg(unix)]
            category: Some("Game".to_string()),
            description: Some(
                "A free and open-source game about the evolution of life.".to_string(),
            ),
            api: Some(ApiType::Github),
            download_link_template:
                "https://github.com/(Revolutionary-Games)/(Thrive)/releases/download/v{version}/Thrive_{version}.0_linux_x11.7z"
                    .to_string(),
            default: true,
            icon: Some("https://revolutionarygamesstudio.com/wp-content/uploads/2016/03/Header-3.png".to_string()),
            ..Default::default()
        }
    }

    /// Returns the default app template 'ProtonUp Qt'
    fn protonup_qt() -> App {
        App {
            name: "ProtonUp Qt".to_string(),
            generic_name: Some("Wine compatibility toolkit".to_string()),
            #[cfg(unix)]
            category: Some("Utility".to_string()),
            description: Some("Install Wine and Proton-based Compatibility Tools.".to_string()),
            api: Some(ApiType::Github),
            download_link_template: "https://github.com/(DavidoTek)/(ProtonUp-Qt)/releases/download/v{version}/ProtonUp-Qt-{version}-x86_64.AppImage".to_string(),
            default: true,
            icon: Some("https://raw.githubusercontent.com/DavidoTek/ProtonUp-Qt/main/pupgui2/resources/img/appicon256.png".to_string()),
            ..Default::default()
        }
    }

    /// Returns the default app template 'Joplin'
    fn joplin() -> App {
        App {
            name: "Joplin".to_string(),
            generic_name: Some("Note taking app".to_string()),
            #[cfg(unix)]
            category: Some("Office".to_string()),
            description: Some("A free, open source note taking and to-do application.".to_string()),
            api: Some(ApiType::Github),
            download_link_template: "https://github.com/(laurent22)/(joplin)/releases/download/v{version}/Joplin-{version}.AppImage".to_string(),
            default: true,
            icon: Some("https://github.com/laurent22/joplin/blob/dev/Assets/LinuxIcons/256x256.png".to_string()),
            ..Default::default()
        }
    }

    /// Returns the default app template 'rust_analyzer(dev)'
    #[cfg(debug_assertions)]
    fn rust_analyzer() -> App {
        App {
            name: "rust-analyzer(dev)".to_string(),
            generic_name: Some("An LSP server".to_string()),
            #[cfg(unix)]
            category: Some("Utility".to_string()),
            description: Some("A Rust LSP server for all your auto-completion needs.".to_string()),
            api: Some(ApiType::Github),
            download_link_template: "https://github.com/(rust-lang)/(rust-analyzer)/releases/download/latest/rust-analyzer-aarch64-unknown-linux-gnu.gz".to_string(),
            default: true,
            ..Default::default()
        }
    }

    /// Returns the default app template 'calibre'
    fn calibre() -> App {
        App {
            name: "Calibre".to_string(),
            generic_name: Some("An eBook manager".to_string()),
            #[cfg(unix)]
            category: Some("Utility".to_string()),
            description: Some("The one stop solution for all your e-book needs.".to_string()),
            api: Some(ApiType::Github),
            download_link_template: "https://github.com/(kovidgoyal)/(calibre)/releases/download/v{version}/calibre-{version}-x86_64.txz".to_string(),
            default: true,
            ..Default::default()
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct App {
    /// Name of the app
    pub name: String,

    /// Generic descriptor for a program, a secondary name, like `Web browser`, `Game`, `IDE`, `Office suit`, and others.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default = "App::return_none")]
    pub generic_name: Option<String>,

    /// A single main [category](https://specifications.freedesktop.org/menu-spec/latest/apa.html#main-category-registry) for each application.
    #[cfg(unix)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default = "App::return_none")]
    pub category: Option<String>,

    /// A short description of a program
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default = "App::return_none")]
    pub description: Option<String>,

    /// Indicates api type (ex.: `Github`). (if none, type `none`, or leave empty)
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default = "App::return_none")]
    pub api: Option<ApiType>,

    /// Web link that downloads the desired object without user intervention.
    ///
    /// Can also be classified as an api in `api` to get access to the latest version number.
    ///
    /// One example is:
    ///
    /// > https://github.com/(FreeTubeApp)/(FreeTube)/releases/download/v{version}-beta/freetube_{version}_amd64.AppImage
    ///
    /// > Note: the '()' are for the program to recognize the repo owner and name (* obligatory)
    ///
    /// > Note: the '{version}' (if any) will be replaced with the app's latest version number
    pub download_link_template: String,

    /// The actual download link after parsing
    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(skip_deserializing)]
    #[serde(default = "String::new")]
    download_link: String,

    /// Specifies the installation methods available for the app
    pub install_methods: InstallMethods,

    /// The current version of the app
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default = "App::return_none")]
    pub version: Option<String>,

    /// Defines if the app is a default app template. (if so, it will accept a duplicate as replacement)
    #[serde(skip)]
    #[serde(default = "App::return_false")]
    default: bool,

    /// Where the '.app' file is stored
    #[serde(skip)]
    #[serde(default = "App::return_none")]
    pub app_location: Option<PathBuf>,

    /// The download link or path to the app icon
    #[serde(default = "App::return_none")]
    pub icon: Option<String>,
}

impl Default for App {
    fn default() -> Self {
        Self {
            name: "".to_string(),
            generic_name: Some("".to_string()),
            #[cfg(unix)]
            category: None,
            description: None,
            api: None,
            download_link: "".to_string(),
            download_link_template: "".to_string(),
            install_methods: InstallMethods(vec![InstallMethod::Executable]),
            version: None,
            default: false,
            app_location: None,
            icon: None,
        }
    }
}

impl App {
    /// Get formatted app name with removed special characters and etc.
    pub fn get_filtered_name(&self) -> String {
        let name = self
            .name
            .replace(' ', "_")
            .replace('!', "")
            .replace('?', "")
            .replace('\"', "")
            .replace('`', "")
            .replace('$', "")
            .replace('#', "");

        let mut new_name = String::new();

        for (index, character) in name.char_indices() {
            if index == 0 {
                new_name.push_str(&character.to_lowercase().to_string());

                continue;
            }

            new_name.push(character);
        }

        new_name
    }

    /// Returns true if both app names are equal.
    pub fn _matches(&self, app: &App) -> bool {
        if self.name == app.name {
            return true;
        }

        false
    }

    /// Get a clone of the download link
    pub fn get_download_link(&self) -> String {
        self.download_link.clone()
    }

    /// Get a mutable reference of the download link
    pub fn get_mut_download_link(&mut self) -> &mut String {
        &mut self.download_link
    }

    /// Returns false
    fn return_false() -> bool {
        false
    }

    /// Returns none
    fn return_none<T>() -> Option<T> {
        None
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(transparent)]
pub struct InstallMethods(Vec<InstallMethod>);

impl InstallMethods {
    /// Returns a vector containing immutable references to all install methods in self
    pub fn get(&self) -> Vec<&InstallMethod> {
        let mut vec = Vec::with_capacity(self.0.len());

        for method in &self.0 {
            vec.push(method);
        }

        vec
    }

    /// Get a vector containing mutable references to all install methods in self
    pub fn _get_mut(&mut self) -> Vec<&mut InstallMethod> {
        let mut vec = Vec::new();

        for method in &mut self.0 {
            vec.push(method);
        }

        vec
    }

    /// Joins together the `InstallMethod`s into a string with the specified separator
    pub fn _join(&self, separator: &str) -> String {
        let mut buffer = String::new();

        for (index, method) in self.0.iter().enumerate() {
            if index == 0 {
                buffer = method.to_string();

                continue;
            }

            buffer += &format!("{}{}", &separator, &method);
        }

        buffer
    }

    /// Checks if self contains no `InstallMethod`s
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
}

/// Enum that represents the different installation methods available
#[derive(Debug, PartialEq, Serialize, Deserialize, Sequence, Clone)]
pub enum InstallMethod {
    /// Download an AppImage and use that as an app
    Executable,
    /// Download git repo, compile (if necessary) and run the app
    Compile,
}

impl InstallMethod {
    pub fn from_str(buffer: &str) -> Option<InstallMethod> {
        let mut string = buffer.trim().to_lowercase();

        string.truncate(1);

        let install_methods: Vec<InstallMethod> = all::<InstallMethod>().collect::<Vec<_>>();

        install_methods.into_iter().find(|install_method| {
            install_method.to_string().to_lowercase().get(0..1).unwrap() == string
        })
    }
}

impl fmt::Display for InstallMethod {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

/// Gets the current apps and app templates from the filesystem.
pub fn get_apps() -> Apps {
    let binding = fetch_internal();
    let internal = binding.borrow();

    match ProjectDirs::from("dev", "installer", "app-installer") {
        Some(project_dirs) => {
            let data_dir = project_dirs.data_local_dir();

            let mut app_entries = match data_dir.read_dir() {
                Ok(app_entries) => app_entries,
                Err(err) => {
                    if err.to_string().contains("os error 2") {
                        if internal.arguments.verbose {
                            eprintln!(
                                "Program directory doesn't exist, reverting to default app templates.\n\
                                \n\
                                Creating program directory...\n"
                            );
                        }

                        match create_dir_all(&data_dir) {
                            Ok(_) => {
                                if internal.arguments.verbose {
                                    eprintln!("Succeeded.\n");
                                }
                            }
                            Err(err) => {
                                if internal.arguments.verbose {
                                    eprintln!(
                                        "Error: {}\n\
                                        \n\
                                        Failed to create program directory.\n",
                                        &err
                                    );
                                }
                            }
                        };
                    } else if internal.arguments.verbose {
                        eprintln!(
                            "Error: {}\n\
                            \n\
                            Failed to get apps from the program directory.\n",
                            &err
                        );
                    }

                    return Apps::default();
                }
            };

            let mut apps = Apps::default();

            'dir_loop: loop {
                for object in app_entries.by_ref() {
                    match object {
                        Ok(entry) => {
                            if entry.path().is_file() {
                                let file = match read_to_string(entry.path()) {
                                    Ok(file) => file,
                                    Err(err) => {
                                        if internal.arguments.verbose {
                                            eprintln!(
                                                "Error: {}\n\
                                                \n\
                                                Failed to get app from the program directory.\n",
                                                &err
                                            );
                                        }

                                        continue;
                                    }
                                };

                                let app = match extract_app(
                                    &file,
                                    &entry.file_name().to_string_lossy().to_string(),
                                ) {
                                    Some(mut app) => {
                                        app.app_location = Some(entry.path());

                                        app
                                    }
                                    None => continue,
                                };

                                match apps.matches(&app) {
                                    Some(index) => {
                                        if apps.0[index].default {
                                            apps.0[index] = app;

                                            continue;
                                        }

                                        if internal.arguments.verbose {
                                            eprintln!(
                                                "Error: {} app has a duplicate; ignoring duplicate.\n",
                                                &apps.0[index].name
                                            );
                                        }

                                        continue;
                                    }
                                    None => {
                                        apps.0.push(app);
                                    }
                                }
                            } else if entry.path().is_dir() {
                                if entry.file_name() == "assets" {
                                    continue;
                                }

                                app_entries = match entry.path().read_dir() {
                                    Ok(dir_entries) => dir_entries,
                                    Err(_) => {
                                        continue;
                                    }
                                };

                                continue 'dir_loop;
                            } else {
                                continue;
                            }
                        }
                        Err(err) => {
                            if err.to_string().contains("os error 2") {
                                if internal.arguments.verbose {
                                    eprintln!(
                                        "App directory doesn't exist.\n\
                                        \n\
                                        Creating app directory...\n"
                                    );
                                }

                                match create_dir_all(&data_dir) {
                                    Ok(_) => {
                                        if internal.arguments.verbose {
                                            eprintln!("Succeeded.");
                                        }
                                    }
                                    Err(err) => {
                                        if internal.arguments.verbose {
                                            eprintln!(
                                                "Error: {}\n\
                                                \n\
                                                Failed to create app directory.\n",
                                                &err
                                            );
                                        }
                                    }
                                };
                            } else if internal.arguments.verbose {
                                eprintln!(
                                    "Error: {}\n\
                                    \n\
                                    Failed to get app from the app directory.\n",
                                    &err
                                );
                            }

                            continue;
                        }
                    }
                }

                break;
            }

            apps
        }

        // Linux:   /home/USER/.local/share/app-installer
        // Windows: C:\Users\USER\AppData\Local\installer\app-installer
        // macOS:   /Users/USER/Library/Application Support/dev.installer.app-installer
        None => {
            eprintln!("Failed to detect program directory, reverting to default app templates.\n");

            Apps::default()
        }
    }
}

/// Extracts the `App` from a string.
pub fn extract_app(string: &str, app_name: &str) -> Option<App> {
    let binding = fetch_internal();
    let internal = binding.borrow();

    match serde_yaml::from_str::<App>(string) {
        Ok(app) => Some(app),
        Err(err) => {
            if internal.arguments.verbose {
                eprintln!(
                    "Error: {}\n\
                    \n\
                    Failed to deserialize the {} app.\n",
                    &err, app_name
                );
            }

            return None;
        }
    }
}

/// Writes app to the app data directory.
pub(crate) fn write_app(app: &mut App) {
    if app.version.is_none() {
        app.version = None;
    } else if app.version.as_ref().unwrap().trim().is_empty()
        || app.version.as_ref().unwrap().trim() == "none"
    {
        app.version = None;
    } else if app.version.as_ref().unwrap().contains(':') {
        app.version = Some(
            app.version
                .as_ref()
                .unwrap()
                .split(':')
                .collect::<Vec<&str>>()[1]
                .trim()
                .to_string(),
        );
    }

    if app.app_location.is_some() {
        write_app_to_file(app, app.app_location.as_ref().unwrap());

        return;
    }

    match ProjectDirs::from("dev", "installer", "app-installer") {
        Some(project_dirs) => {
            let data_dir = project_dirs.data_local_dir();
            let app_name = app.get_filtered_name();

            let mut app_dir = data_dir.join(&app_name);
            let mut app_file: PathBuf;

            loop {
                if app_dir.is_dir() {
                    app_file = app_dir.join({
                        if cfg!(unix) {
                            app_name.clone() + ".app"
                        } else {
                            app_name.clone() + ".txt"
                        }
                    });

                    if app_file.exists() {
                        app_dir = app_file;

                        continue;
                    }

                    write_app_to_file(&app, &app_file);

                    break;
                } else {
                    write_app_to_file(&app, {
                        if cfg!(unix) {
                            app_dir.set_extension("app");

                            &app_dir
                        } else {
                            app_dir.set_extension("txt");

                            &app_dir
                        }
                    });

                    break;
                }
            }
        }

        // Linux:   /home/USER/.local/share/app-installer
        // Windows: C:\Users\USER\AppData\Local\installer\app-installer
        // macOS:   /Users/USER/Library/Application Support/dev.installer.app-installer
        None => {
            eprintln!("Failed to write app to program directory.\n");
        }
    }
}

/// Writes app to file.
pub fn write_app_to_file(app: &App, path: &Path) {
    if let Err(err) = write(
        path,
        serde_yaml::to_string(&app.clone()).unwrap_or_else(|err| {
            eprintln!(
                "Error: {}\n\
                \n\
                Failed to serialize the {} app into the yaml format.",
                &err, &app.name
            );

            exit(exitcode::UNAVAILABLE);
        }),
    ) {
        eprintln!(
            "Error: {}\n\
            \n\
            Failed to write the {} app file to app directory.",
            &err, &app.name
        );

        exit(exitcode::CANTCREAT);
    };
}
