use std::{fmt, io::Read, process::exit, str::FromStr};

use console::style;
use enum_iterator::{all, Sequence};
use reqwest::{header::USER_AGENT, Method};
use serde::{Deserialize, Serialize};

use crate::process::fetch_internal;

/// Enum that represents the different types of APIs that are supported
#[derive(Debug, PartialEq, Serialize, Deserialize, Sequence, Clone)]
pub enum ApiType {
    /// Uses Github's [REST API](https://docs.github.com/fr/rest)
    Github,
}

impl ApiType {
    pub fn _from_str(buffer: &str) -> Option<ApiType> {
        let string = buffer.trim().to_lowercase();

        let api_types: Vec<ApiType> = all::<ApiType>().collect::<Vec<_>>();

        api_types
            .into_iter()
            .find(|api_type| string == api_type.to_string().to_lowercase())
    }
}

impl fmt::Display for ApiType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

/// Calls an API and returns the response as a `String`.
fn call_api(api: reqwest::Url) -> String {
    let response = reqwest::blocking::Client::new()
        .request(Method::GET, api.clone())
        .header(USER_AGENT, "App-Installer")
        .send();

    match response {
        Ok(mut response) => {
            let mut buffer = String::new();

            if let Err(err) = response.read_to_string(&mut buffer) {
                eprintln!(
                    "Error: {}\n\
                    \n\
                    Response from api '{}' is not a valid string.",
                    &err, &api
                );

                exit(exitcode::PROTOCOL);
            };

            buffer
        }
        Err(err) => {
            eprintln!(
                "Error: {}\n\
                \n\
                Failed to fetch from api '{}'.",
                &err, &api
            );

            exit(exitcode::TEMPFAIL);
        }
    }
}

/// Gets and returns the latest released versions of a Github repo
pub fn get_latest_github_version(repo_owner: &str, repo_name: &str) -> String {
    let binding = fetch_internal();
    let internal = binding.borrow();

    // Kind of follows Github's required [template](https://docs.github.com/fr/rest/releases/releases?apiVersion=2022-11-28#get-the-latest-release).
    let api = format!(
        "https://api.github.com/repos/{}/{}/releases",
        &repo_owner, &repo_name
    );

    let response = call_api(reqwest::Url::from_str(&api).unwrap_or_else(|err| {
        eprintln!(
            "Error: {}\n\
            \n\
            Default api '{}' is not a valid url.",
            &err, &api
        );

        exit(exitcode::DATAERR);
    }));

    let mut tag_name = String::new();

    for line in response.split(',') {
        if line.contains("\"tag_name\":") {
            if internal.arguments.debug {
                dbg!(&line);
            }

            tag_name = line.split('\"').collect::<Vec<&str>>()[3]
                .trim()
                .to_string();

            break;
        }
    }

    if internal.arguments.debug {
        dbg!(&tag_name);

        eprintln!();
    }

    let tag_lenght = tag_name.len() - 1;
    let mut version = String::with_capacity(tag_lenght);

    // Trims chracters and/or points surrounding the version number
    for (index, character) in tag_name.chars().enumerate() {
        if (character.is_numeric() || character == '.')
            && (index != tag_lenght || character.is_numeric())
        {
            version.push(character);
        }
    }

    if version.is_empty() {
        eprintln!(
            "Found no version number in latest release tag '{}'.\n",
            &tag_name
        );

        let mut buffer = String::new();

        println!(
            "Do you want to proceed? {}y/{}{}:",
            style("[").bold(),
            style("N").yellow(),
            style("]").bold()
        );

        crate::io::get_lower(&mut buffer, false, true);

        if buffer != "y" {
            exit(exitcode::OK);
        }

        println!();
    }

    version
}
