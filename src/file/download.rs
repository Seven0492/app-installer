use crate::{file::config::Config, io, process::fetch_internal};
use std::cmp::min;
use std::fs::read;
use std::io::{Seek, Write};
use std::path::Path;
use std::{
    env::temp_dir,
    fs::{remove_dir_all, remove_file},
    path::PathBuf,
    process::exit,
};

use console::style;
use exitcode::ExitCode;
use futures_util::StreamExt;
use indicatif::{ProgressBar, ProgressStyle};
use reqwest::ClientBuilder;

use super::apps::App;

/// Downloads file from `link` to `download_location` in `config`,
/// or a temporary directory, and returns the path to it
pub(crate) fn download_in_temp(app: &mut App, config: &Config) -> Result<PathBuf, ExitCode> {
    let link = &app.get_download_link();
    
    let temporary_dir = config
        .download_location
        .clone()
        .unwrap_or(temp_dir().display().to_string());

    let mut temporary_dir = PathBuf::from(temporary_dir);

    println!("\nDownload directory is: \"{}\"", &temporary_dir.display());

    println!("\nDo you want to change the download directory? {}y/{}{}:", style("[").bold(), style("N").yellow(), style("]").bold());

    let mut buffer = String::new();

    io::get_lower(&mut buffer, false, false);

    if &buffer == "y" || &buffer == "yes" {
        println!();

        temporary_dir = input_dir();
    }

    match temporary_dir.try_exists() {
        Ok(available) => {
            if available {
                download_file(link, temporary_dir, app)
            // If directory is available but contains broken symbolic links.
            } else {
                eprintln!(
                    "\nTemporary directory '{}' has broken symbolic links.\n",
                    temporary_dir.display()
                );

                ask_for_dir(link, app)
            }
        }
        // If temporary directory is unavailable/unaccessible.
        // (ex.: lack of necessary permissions)
        Err(err) => {
            eprintln!(
                "\nError: {}\n\
                \n\
                Temporary directory '{}' is unavailable.\n",
                &err,
                &temporary_dir.display()
            );

            ask_for_dir(link, app)
        }
    }
}

/// Downloads file from `link` to directory in `dir`
fn download_file(link: &str, dir: PathBuf, mut app: &mut App) -> Result<PathBuf, ExitCode> {
    let mut path = dir.clone();

    let file_name = link.split('/').last().unwrap_or(&app.name);

    path.push(file_name.clone());

    println!();

    if path.is_file() || path.is_symlink() {
        if let Err(err) = remove_file(&path) {
            eprintln!(
                "\nError: {}\n\
                \n\
                Failed to remove pre-existing {} file.",
                &err, &app.name
            );

            exit(exitcode::CANTCREAT);
        }
    } else if path.is_dir() {
        if let Err(err) = remove_dir_all(&path) {
            eprintln!(
                "\nError: {}\n\
                \n\
                Failed to remove pre-existing {} directory.",
                &err, &app.name
            );

            exit(exitcode::CANTCREAT);
        }
    }
    
    let file_path = match download(link, &dir, &mut app, false) {
        Ok(path) => path,
        Err(err) => {
            return Err(err);
        }
    };
    
    let file = match read(&file_path) {
        Ok(file) => file,
        Err(err) => {
            eprintln!(
                "Error: {}\n\
                \n\
                Failed to read from downloaded file.",
                &err
            );
            
            return Err(exitcode::NOINPUT);
        }
    };
    
    if file == b"Not Found" {
        eprintln!(
            "Error: downloaded file only contains 'Not Found'.\n\
            \n\
            File '{}' doesn't exist.",
            &link
        );
        
        return Err(exitcode::NOINPUT);
    }

    Ok(file_path)
}

pub fn download(link: &str, path: &Path, current_app: &mut App, is_asset: bool) -> Result<PathBuf, ExitCode> {
    let binding = fetch_internal();
    let internal = binding.borrow();
    
    let mut path = path.to_path_buf();
    
    if path.is_dir() {
        path.push(link.split('/').collect::<Vec<&str>>().last().unwrap());
    }
    
    let url_protocol = link.split(':').collect::<Vec<&str>>()[0];
    
    if internal.arguments.debug {
        dbg!(&path);
        dbg!(&link);
        dbg!(&url_protocol);
        
        println!();
    }
    
    match url_protocol {
        "http" | "https" => {
            let tokio_runtime = tokio::runtime::Runtime::new().unwrap();
            let future = http_download(
                &current_app,
                link,
                path,
                is_asset);
            
            match tokio_runtime.block_on(future) {
                Ok(path) => {
                    return Ok(PathBuf::from(path));
                },
                Err(exit_code) => {
                    return Err(exit_code);
                }
            }
        },
        "file" => {
            let path = link.split(':').collect::<Vec<&str>>()[1];
            
            return Ok(PathBuf::from(path.split_once('/').unwrap().1));
        },
        protocol => {
            eprintln!(
                "Error: '{}' is an unsupported protocol.\n\
                \n\
                Failed to download from link '{}'.",
                &protocol,
                &link
            );
            
            return Err(exitcode::DATAERR);
        }
    }
}

/// Asks for input, and if it's a valid path, download file.
fn ask_for_dir(link: &str, mut app: &mut App) -> Result<PathBuf, ExitCode> {
    let mut dir = input_dir();
    
    match dir.try_exists() {
        // If path is valid
        Ok(true) => {
            return download_file(link, dir, &mut app);
        }
        // If invalid
        _ => {
            loop {
                eprintln!("\nInvalid path! Try again:");

                dir = input_dir();

                match dir.try_exists() {
                    // Valid
                    Ok(true) => {
                        return download_file(link, dir, &mut app);
                    }
                    // Invalid
                    _ => continue,
                }
            }
        }
    }
}

/// Asks for input and returns the converted PathBuf
fn input_dir() -> PathBuf {
    let mut buffer = String::new();

    println!("Please enter download directory:");

    io::get_lower(&mut buffer, true, false);

    PathBuf::from(buffer)
}

pub async fn http_download(app: &App, url: impl Into<String>, path: impl Into<PathBuf>, is_asset: bool) -> Result<String, ExitCode> {    
    let url: String = url.into();
    let path: PathBuf = path.into();
    let mut download_path = path.clone();
    
    download_path.set_extension(".part");
    
    let binding = fetch_internal();
    let internal = binding.borrow();
    
    if internal.arguments.debug {
        dbg!(&url);
        dbg!(&path);
        
        println!();
    }
    
    let client = match ClientBuilder::new().user_agent("app-installer").build() {
        Ok(client) => client,
        Err(err) => {
            eprintln!(
                "Error: {}\n\
                \n\
                Failed to build `reqwest` client. (internal error; report to maintainer)",
                &err
            );
            
            return Err(exitcode::SOFTWARE);
        }
    };
    
    let response = match client.get(&url).send().await {
        Ok(response) => response,
        Err(err) => {
            eprintln!(
                "Error: {}\n\
                \n\
                Failed to download file.",
                &err
            );

            return Err(exitcode::UNAVAILABLE);
        }
    };

    let response_lenght = match response
        .content_length() {
            Some(lenght) => lenght,
            None => {
                eprintln!("Error: the lenght of the response is unknown.");
                
                return Err(exitcode::UNAVAILABLE);
            }
        };

    let mut download_file;
    let mut file;
    let mut downloaded: u64 = 0;
    let mut stream = response.bytes_stream();
    
    if std::path::Path::new(&download_path).exists() {
        download_file = std::fs::OpenOptions::new()
            .read(true)
            .append(true)
            .open(&path)
            .unwrap();

        let file_size = std::fs::metadata(&path).unwrap().len();

        download_file.seek(std::io::SeekFrom::Start(file_size)).unwrap();

        downloaded = file_size;
        
        file = match super::new_file(&path) {
            Ok(file) => file,
            Err(exit_code) => {
                return Err(exit_code);
            }
        };
    } else {
        download_file = match super::new_file(&download_path) {
            Ok(file) => file,
            Err(exit_code) => {
                return Err(exit_code);
            }
        };
        
        file = match super::new_file(&path) {
            Ok(file) => file,
            Err(exit_code) => {
                return Err(exit_code);
            }
        };
    }
    
    let progress_bar = ProgressBar::new(response_lenght);
    
    let progress_bar_style = match ProgressStyle::default_bar()
            .template("{msg} {spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan}] {bytes_per_sec} {eta} ({bytes}/{total_bytes})") {
        Ok(template) => template,
        Err(err) => {
            eprintln!(
                "Error: {}\n\
                \n\
                Failed to interpolate progress bar template string.",
                &err
            );
                    
            return Err(exitcode::SOFTWARE);
        }
    }.progress_chars("=> ");

    progress_bar.set_style(progress_bar_style);

    if is_asset {
        progress_bar.set_message(format!("{}",
        url.split('/').collect::<Vec<&str>>().last().unwrap(),
    ));
    } else {
    progress_bar.set_message(format!("{}{}",
        &app.name,
        {
            let mut version = app.version.as_ref().unwrap_or(&"".to_string()).trim().to_string();
            
            if version == "none" || version.is_empty() {
                version = "".to_string();
            } else {
                if version.contains("behind:") {
                    version = version.split(':').collect::<Vec<&str>>()[2].to_string();
                } else {
                    version = version.split(':').collect::<Vec<&str>>()[1].trim().to_string();
                }
                
                version = format!(" ({})", version);
            }
            
            version
        }
    ));
    }

    while let Some(item) = stream.next().await {
        let chunk = match item {
            Ok(item) => item,
            Err(err) => {
                eprintln!("\nError: {}", &err);
                
                return Err(exitcode::UNAVAILABLE);
            }
        };

        if let Err(err) = download_file.write(&chunk) {
            eprintln!(
                "\nError: {}\n\
                \n\
                Failed to write to file.",
                &err
            );

            return Err(exitcode::CANTCREAT);
        };

        let new = min(downloaded + (chunk.len() as u64), response_lenght);

        downloaded = new;

        progress_bar.set_position(new);
    }

    progress_bar.finish();
    
    if let Err(err) = file.write_all(&read(&download_path).unwrap()) {
        eprintln!(
            "Error: {}\n\
            \n\
            Failed to write downloaded file to download location.\n",
            &err
        );
        
        return Err(exitcode::CANTCREAT);
    }
    
    if let Err(err) = remove_file(download_path) {
        if internal.arguments.verbose {
            eprintln!(
                "Error: {}\n\
                \n\
                Failed to remove download artifact.\n",
                &err
            );
        }
    }

    if internal.arguments.verbose {
        println!("\n\nDownloaded {} to '{}'\n", &app.name, &path.display());
    } else {
        println!("\n");
    }

    Ok(path.to_str().expect("Expected valid UTF8 in icon path.").to_string())
}
