use std::process::Command;

use crate::process::fetch_internal;

/// Refreshes the desktop environment's cache to
/// synchronize changes with the currently running session
#[cfg(unix)]
pub fn refresh_cache() {
    let binding = fetch_internal();
    let internal = binding.borrow();

    let child;

    child = Command::new("xdg-desktop-menu").arg("forceupdate").spawn();

    match child {
        Ok(mut child) => {
            let status = child.wait();

            if let Err(err) = status {
                if internal.arguments.verbose {
                    eprintln!(
                        "\nError: {}\n\
                        \n\
                        Failed to refresh desktop cache.\n",
                        &err
                    );
                }

                return;
            }

            if let Ok(exit_code) = status {
                if exit_code.success() {
                    if internal.arguments.verbose {
                        eprintln!("\nSuccessfully refreshed desktop cache.\n");
                    }

                    return;
                }

                eprintln!(
                    "\nError: {}\n\
                    \n\
                    Failed to refresh desktop cache.\n",
                    &exit_code
                );
            }
        }
        Err(err) => {
            eprintln!(
                "\nError: {}\n\
                \n\
                Failed to refresh desktop cache.\n",
                &err
            );
        }
    }
}
