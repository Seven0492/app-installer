use std::process::exit;

pub mod desktop;

/// Warns a user they are on an unsupported platform and exits the program
pub fn warning() {
    if cfg!(windows) {
        println!(
            "You are on Windows!\
            \n\
            \n\
            Fortunately (or unfortunately), you do not need to depend on this script, as\n\
            osu!lazer has an official Windows installer on their Github page:\n\
            --> https://github.com/ppy/osu/releases/latest/download/install.exe\
            \n\
            \n\
            This program was mainly made with Linux in mind,\n\
            since there is no official installer for it, and so many ways to install,\n\
            I wanted to simplify it!\
            \n\
            \n\
            But, if there are any hicks or improvements on Windows you might be interested in,\n\
            let me know! It'll be a fun challenge! But do note that I, the author, am on Linux.\n\
            So any development concerning Windows might take a bit longer (but possible)."
        );
    } else {
        println!(
            "You are on an unsupported OS!\
            \n\
            \n\
            If you need any sort of documentation to help you set up osu!lazer on your\n\
            platform, I would recommend googling or exploring it's official Github page:\n\
            --> https://github.com/ppy/osu\
            \n\
            \n\
            This program was mainly made with Linux in mind,\n\
            since there is no official installer for it, and so many ways to install,\n\
            I wanted to simplify it!\
            \n\
            \n\
            But, if you are on a Unix-like or MacOs platform,\n\
            and you're willing to inform me about it\n\
            (ex.: quirks, directory structure,\n\
            if it has a specific app manager which might be worth pushing to, etc),\n\
            I'm open to the idea!\n\
            But only with your cooperation can I do it, I am doing this on my free time, after all."
        );
    }

    exit(exitcode::UNAVAILABLE);
}
